import random
tableLimit = 500
minBet = 5
runNumber = 100
roundsPerRun = 1000

def main():
    f = open('result.csv', 'w')
    for x in range(0,runNumber):
        newBet = minBet
        runMoney = 0
        for y in range(0, roundsPerRun):
            breakFlag = False
            randomNumber = random.randint(1,38)
            if randomNumber <= 18:
                runMoney = runMoney + newBet
                newBet = minBet
            else:
                runMoney = runMoney - newBet
                if (newBet * 2) > tableLimit:
                    newBet = minBet
                    breakFlag = True
                    
                else:
                    newBet = newBet * 2
            
            if breakFlag:
                break
            
        f.write(str(x) + "," + str(runMoney) + "\n")
        
    f.close()
    
main()
        
                    